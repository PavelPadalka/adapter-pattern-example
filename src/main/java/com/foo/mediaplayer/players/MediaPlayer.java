package com.foo.mediaplayer.players;

public interface MediaPlayer {
    public void play(String audioType, String fileName);
}
